#Dieser Skript erstellt die Dateinen index.php und Docker und erstellt danach basierend auf diesen Dateinen ein Image in der Registry

#!/bin/bash

if [ $# -eq 0 ]
then
  echo "Kein Argument für den Imagenamen angegeben."

else

  #index.php erstellen

  echo "<?php echo '<p>Web Server im Container ;-)</p>'; ?>" > ./index.php

  #Dockerfile erstellen

  echo "FROM public.ecr.aws/docker/library/php:7.4-apache" > ./Dockerfile
  echo "COPY index.php /var/www/html/" >> ./Dockerfile

  #Docker image erstellen
  echo "image name = " $1

  docker login registry.gitlab.com
  docker build -t registry.gitlab.com/18remo96/m169-services/$1 .
  docker push registry.gitlab.com/18remo96/m169-services/$1

fi



